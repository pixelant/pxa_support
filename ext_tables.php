<?php
if (!defined ('TYPO3_MODE')) {
	die ('Access denied.');
}

$tmp_confArr = unserialize( $GLOBALS['TYPO3_CONF_VARS']['EXT']['extConf']['pxa_support'] );

if (TYPO3_MODE == 'BE' && $tmp_confArr['supportUrl'] != '') {
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addModulePath('help_txpxasupportM1', \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'mod1/');
		
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addModule('help', 'txpxasupportM1', 'top', \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'mod1/');
}


if (TYPO3_MODE == 'BE' && $tmp_confArr['manualUrl'] != '') {
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addModulePath('help_txpxasupportM2', \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'mod2/');
		
	\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addModule('help', 'txpxasupportM2', 'top', \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extPath($_EXTKEY) . 'mod2/');
}
?>
