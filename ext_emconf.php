<?php

########################################################################
# Extension Manager/Repository config file for ext "pxa_support".
#
# Auto generated 01-06-2011 16:31
#
# Manual updates:
# Only the data in the array - everything else is removed by next
# writing. "version" and "dependencies" must not be touched!
########################################################################

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Support',
	'description' => 'Adds two BE Help modules, one for support and one for manual',
	'category' => 'module',
	'author' => 'erik dahlin',
	'author_email' => 'erik@pixelant.se',
	'shy' => '',
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'module' => '',
	'state' => 'stable',
	'internal' => '',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'author_company' => 'Pixelant',
	'version' => '1.0.0',
	'constraints' => array(
		'depends' => array(
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
	'_md5_values_when_last_written' => 'a:17:{s:9:"ChangeLog";s:4:"ff8d";s:10:"README.txt";s:4:"ee2d";s:21:"ext_conf_template.txt";s:4:"5259";s:12:"ext_icon.gif";s:4:"80c4";s:14:"ext_tables.php";s:4:"1597";s:19:"doc/wizard_form.dat";s:4:"2f47";s:20:"doc/wizard_form.html";s:4:"acd3";s:13:"mod1/conf.php";s:4:"e7ef";s:14:"mod1/index.php";s:4:"5987";s:18:"mod1/locallang.xml";s:4:"1bb8";s:22:"mod1/locallang_mod.xml";s:4:"ef0a";s:19:"mod1/moduleicon.gif";s:4:"80c4";s:13:"mod2/conf.php";s:4:"482d";s:14:"mod2/index.php";s:4:"c52d";s:18:"mod2/locallang.xml";s:4:"97be";s:22:"mod2/locallang_mod.xml";s:4:"e79a";s:19:"mod2/moduleicon.gif";s:4:"5500";}',
	'suggests' => array(
	),
);

?>